#!/usr/local/bin/python3.8

""" Takes input filter file and creates unqiue list of the used colors

Calling:
    ./color_strip.py <file.filter> <out.json>

Input:
    <file.filter>: the filter file to be stripped
    <out.json>: output file for unique colors json

Notes:
    + There are some unnamed colors which do not seem to change between presets
        - I have chosen to ignore these colors
    + There are some colors tagged with the incorrect type in the comment
        - e.g. "SetBackgroundColor 0 0 0 255             # BORDERCOLOR:	 Neutral T1"

IMPORTANT:
    + Although this script performs the action correctly, the number of poor naming conventions used within the filter result in this not being a viable approach or general idea.
"""

import json
import re
import sys

keywords = {
    'text': 'SetTextColor',
    'border': 'SetBorderColor',
    'bg': 'SetBackgroundColor'
}

colors = {
    'text': {},
    'border': {},
    'bg': {}
}

def clean_colors(color_data, sort_code):
    for item in color_data:
        tmp = item.split("#")
        if len(tmp) == 2:
            key = ":".join(tmp[1].split(":")[1:]).strip()
            if key not in colors[sort_code]:
                colors[sort_code][key] = item

def main():
    # checking for the command line arguments
    if len(sys.argv) != 3:
        print("Invalid function call! Please use: ./color_strip.py <file.filter> <out.json>")

    filter_file = sys.argv[1]
    output = sys.argv[2]

    # loading the file into memory
    with open(filter_file, "r") as ffd:
        data = [re.sub("^#", "", line).strip() for line in ffd]

    # grabbing useful data
    text_color = [line for line in data if keywords['text'] in line]
    border_color = [line for line in data if keywords['border'] in line]
    bg_color = [line for line in data if keywords['bg'] in line]

    # cleaning data of unnamed lines and uniquifying it
    clean_colors(text_color, 'text')
    clean_colors(border_color, 'border')
    clean_colors(bg_color, 'bg')

    # creating output file
    with open(output, "w") as output_fd:
        json.dump(colors, output_fd)


if __name__ == "__main__":
    main()
