#!/usr/local/bin/python3.8

""" Takes all seed filters to generate versioned output

Calling:
    ./gen_version.py <config.json> <seed_folder> <default_sounds_folder> <output_folder>

Input:
    <config.json>: configuration file for the function
    <seed_folder>: folder containing all seed filters for processing
        --> Seed folder should also contain the README.txt file to be included in the final output
    <default_sounds_folder>: folder containing the 6 default sound clips for custom sound filters
    <output_folder>: folder where functions outputs will exist
"""

sound_keyword = "PlayAlertSound"

sound_keys = {
    "1": "1maybevaluable.mp3",
    "2": "2currency.mp3",
    "3": "3uniques.mp3",
    "4": "4maps.mp3",
    "5": "5highmaps.mp3",
    "6": "6veryvaluable.mp3"
}

from shutil import copy

import json
import os
import re
import sys

def main():
    # checking for the command line arguments
    if len(sys.argv) != 5:
        print("Invalid function call! Please use: ./gen_version.py <config.json> <seed_folder> <default_sounds_folder> <output_folder>")
        sys.exit(1)

    config_file = sys.argv[1]
    seed_folder = sys.argv[2]
    sound_folder = sys.argv[3]
    output_folder = sys.argv[4]

    # parsing the config file
    with open(config_file, "r") as config_fd:
        config = json.load(config_fd)

    # creating the output directory
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    # grabbing a list of files that need to be processed
    seed_filters = [f for f in os.listdir(seed_folder) if os.path.isfile(os.path.join(seed_folder, f))]

    # processing each of the files
    for file_name in seed_filters:
        if file_name == "README.txt":
            # README file just need to be copied into the output directory; no further processing
            copy(os.path.join(seed_folder, file_name), output_folder)
            continue
        elif file_name[0] == ".":
            # ignoring hidden/system files
            continue

        # processing for filter files
        with open(os.path.join(seed_folder, file_name), "r") as f:
            file_data = [line.strip() for line in f]

        for index, line in enumerate(file_data):
            # check if the line is for playing a sound effect
            if sound_keyword in line:
                # grabbing the number of the sound being played
                sound_type = line.split(sound_keyword)[1].strip().split(" ")[0]

                # checking if it is a comment line
                comment = False
                if line[0] == "#":
                    comment = True

                # creating the new line
                new_line = []
                if comment:
                    new_line.append("# ")
                new_line.append("CustomAlertSound")
                new_line.append("\"sounds/" + sound_keys[sound_type] + "\"")
                file_data[index] = " ".join(new_line)

        # bookkeeping for file names
        fname = file_name.split(".")
        fname[0] += "_" + config["version"]

        # copying the original file
        copy(os.path.join(seed_folder, file_name), os.path.join(output_folder, ".".join(fname)))

        # writing the custom sound file
        cs_fname = fname
        cs_fname[0] += "_cs"

        with open(os.path.join(output_folder, ".".join(cs_fname)), "w") as csfd:
            for line in file_data:
                csfd.write(line + "\n")


    # dealing with sound files
    sound_output = os.path.join(output_folder, "sounds")
    if not os.path.exists(sound_output):
        os.makedirs(sound_output)

    for sound_file in os.listdir(sound_folder):
        if os.path.isfile(os.path.join(sound_folder, sound_file)):
            copy(os.path.join(sound_folder, sound_file), sound_output)


if __name__ == "__main__":
    main()
