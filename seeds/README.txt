*******************************
**    Ario's Loot Filters    **
**      Version: 0.4.0       **
**       Style: Velvet       **
*******************************

This filter is based off of NeverSink's filter suite.
For the most part, filters are left unchanged expect the changes documented below

+ All Talismans are hidden
+ Some incursion mods are hidden


*******************************
****    CURRENCY FILTER    ****
*******************************

The currency filter is based off of the NeverSink Strict filter.
The currency filter is maintained separately from the rest of the filters offered in this pack.

+ Shows chaos and regal recipe items
+ Shows all chromatic worthy items expect large ones (i.e. no 2x4 items)
+ Disables all Talismans
+ Hides some incursion modifiers
+ Shows lower quality gems for Gem Cutter Prisms recipe
+ Shows utility flasks (in general and with quality)


*******************************
*****    CUSTOM SOUNDS    *****
*******************************

All filters in this pack now support custom sounds!
In order to get custom sounds working, do the following:
    1) Move the included "sounds" folder into the same directory you place the filters
    2) In game, select a filter with the "_cs" extension for custom sounds

The sounds located within the sounds folder may be replaced, only the names need to be maintained.


*******************************
*******    CHANGELOG    *******
*******************************
Version 0.4.0
    + Updated filter for patch 3.11
    + Enabled Incantation card drops
    + Reverted tiering changes to Delirium Orbs
    + Revered changes to Offering to the Goddess
        - Need to figure out a way to hide these in Uber Strict alone

Version 0.3.7
    + Updated to latest softcore economy version of NeverSink base filters
    + Reduced size of Offering to the Goddess drops
    + Moved Teal and Verdant oil to be shown on all filters

Version 0.3.6
    + Updated to latest softcore economy version of Neversink base filters
    + Removed special highlighting for 5-linked items (still should show under other applicable rules)
    + Moved "The Incantation" card from medium to low tier
    + Moved Diviner's Delirium Orbs into the Exalt-type tier (mostly for special highlighting)
    + Disabled special crafting bases below level 84 (except for currency filter)
    + Disabled low-level influenced bases (except for currency filter)
    + Re-enabled some Incursion modifiers that show promise for profit

Version 0.3.5
    + Another update from NeverSink side
    + Officially added support for custom sounds
    + Officially added support for a crafting/currency filter

Version 0.3.4
    + Secondary update for 3.10 and the Delirium league shortly after it started
    + No updates made on my end, just from NeverSink

Version 0.3.3
    + Updated format of the README.txt file and the version ordering of the CHANGELOG
    + Updated the filter to support 3.10 content
    + Relaxed a lot of constraints previously placed in the file (much more faith in NeverSink)

Version 0.2.3
    + Made more Vaal gems hidden

Version 0.2.2
    + Fixed misconfiguration where incursion modifiers were still being shown
        - Kept summoner modifiers for staffs shown
        - Kept incursion modified maps shown with a special template

Version 0.2.1
    + Fixed problem where I made ArioVStrict basically == ArioVStrictPlus
    + Further updated orb prices
    + Updated all filters to show more essences (all for ArioVStrictPlus)
    + Added rule to show high level (82+) rare jewels
    + Re-enabled 6-socket vendor recipe for ArioVStrict
    + Enabled 20% quality flasks to be shown for ArioVStrict
    + Updated some README.txt file formatting and phrasing

Version 0.2.0
    + Added README file with changelog
    + Removed ArioStrict version
        - General rare bases (even high level) are selling for 1-4 chaos --> not worth the time to pick up, sort, and sell
    + Created ArioVStrictPlus version
        - Occasionally may have need to farm all forms of currency (specifically chromatic orbs)
    + Enabled smaller Perandus Coin stacks to be shown
    + Disabled all magic and rare jewels (exception -- prismatics)
    + Disabled rare and magic items with league specific modifiers
    + Modified some currency valuations
    + Modified some divination card valuations
    + TODO: Figure out map ilvls to determine what "outleveled" exactly means
